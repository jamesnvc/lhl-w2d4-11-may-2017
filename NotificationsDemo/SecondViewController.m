//
//  SecondViewController.m
//  NotificationsDemo
//
//  Created by James Cash on 11-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UITextField *enteredTextField;
@property (weak, nonatomic) IBOutlet UIButton *finishedButton;

@property (nonatomic,strong) NSString *enteredText;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.finishedButton addTarget:self
                            action:@selector(dismissView:)
                  forControlEvents:UIControlEventTouchUpOutside];
    [self.finishedButton addTarget:self
                            action:@selector(dismissView:)
                  forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)dismissView:(id)sender {
    self.enteredText = self.enteredTextField.text;
    /*
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TextEnteredNotification"
     object:self
     userInfo:@{@"enteredText": self.enteredTextField.text}];
     */
    [self.enteredTextField resignFirstResponder];
    [self dismissViewControllerAnimated:YES
                             completion:^{}];
}

@end
