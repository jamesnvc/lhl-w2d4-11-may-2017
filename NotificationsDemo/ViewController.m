//
//  ViewController.m
//  NotificationsDemo
//
//  Created by James Cash on 11-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *enteredTextLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(gotTheText:)
     name:@"TextEnteredNotification"
     object:nil];
     */

//    self.view.backgroundColor
// keypath: @"view.backgroundColor"

    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressedBody:)];
    self.enteredTextLabel.userInteractionEnabled = YES;
    [self.enteredTextLabel addGestureRecognizer:longPress];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"secondViewSegue"]) {
        UIViewController *second = segue.destinationViewController;
        [second addObserver:self
                 forKeyPath:@"enteredText"
                    options:NSKeyValueObservingOptionNew
                    context:(void*)@"some string"];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    NSLog(@"%@ %@ %@ %@", keyPath, object, change, context);
    self.enteredTextLabel.text = change[NSKeyValueChangeNewKey];
    [object removeObserver:self forKeyPath:@"enteredText"];
}

- (void)gotTheText:(NSNotification*)notification
{
    NSLog(@"Got notification: %@", notification);
    self.enteredTextLabel.text = notification.userInfo[@"enteredText"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)longPressedBody:(UILongPressGestureRecognizer *)sender {
    CGPoint location = [sender locationInView:self.view];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            NSLog(@"event started %@", NSStringFromCGPoint(location));
            break;
        case UIGestureRecognizerStateChanged:
            NSLog(@"event moved %@", NSStringFromCGPoint(location));
            break;
        case UIGestureRecognizerStateEnded:
            NSLog(@"finished %@", NSStringFromCGPoint(location));
            break;
        default:
            break;
    }
}

@end
